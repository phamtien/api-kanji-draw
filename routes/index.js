var express = require('express');
var router = express.Router();
var $ = require('jquery')(require('jsdom-no-contextify').jsdom().parentWindow);
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/kanji', function(req, res, next){
  var rqUrl = "https://inputtools.google.com/request?itc=ja-t-i0-handwrit&app=translate";
  var dataRq = new Object();
  var request = req.body;
  dataRq.api_level = "537.36";
  dataRq.app_version = 0.4;
  dataRq.device = "5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
  dataRq.input_type = 0;
  dataRq.options = "enable_pre_space";
  dataRq.requests = [];
  dataRq.requests[0] = request;

  $.ajax({
      type	: 'POST',
      url	: rqUrl,
      contentType : "application/json; charset=UTF-8",
      data	: JSON.stringify(dataRq),
      dataType : 'json',
      success : function(data, status, xhr) {
          res.json(data);
      }.bind(this),
      error	: function(xhr, status, xhr) {
        res.json(xhr);
      }.bind(this)
  });

})

module.exports = router;
